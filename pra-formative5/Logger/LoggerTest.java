import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerTest {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(LoggerTest.class.getName());
        Scanner input = new Scanner(System.in);

        System.out.print("Silakan masukan angka pertama : ");
        String a = input.nextLine();
        System.out.print("Silakan masukan angka kedua: ");
        String b = input.nextLine();

        try {
            int c = Integer.parseInt(a);
            int d = Integer.parseInt(b);
            int result = c / d;
            System.out.println("Hasil : " + result);
            logger.info("Hello INFO " + LoggerTest.class.getName());
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE, "Hello SEVERE" + LoggerTest.class.getName());
            logger.info("Hello INFO " + LoggerTest.class.getName());
        }
    }
}

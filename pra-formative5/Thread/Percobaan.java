import java.util.Random;

class Percobaan extends Thread {

    public Percobaan(String id) {
        super(id);
    }
    @Override
    public void run() {
        Random ran = new Random();
        int i = ran.nextInt(1000);
        try {
            Thread.sleep((long) i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread " + this.getId() +" is running" + " ");

    }
}